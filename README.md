# 200OK Badges

Generate badges for 200OK conference.

- Sample Google Sheet: https://docs.google.com/spreadsheets/d/16O5SsLZ--bWBUzK2geTAj-X4ngmrOPawf1LOwduYxSE
- Sample Output:
  - [Attendees.pdf](templates/2019/sample/Attendees.pdf)
  - [Speakers.pdf](templates/2019/sample/Speakers.pdf)
  - [Volunteers.pdf](templates/2019/sample/Volunteers.pdf)

## Dependencies

- Make
- Python 3.7.3 ([pyenv](https://github.com/pyenv/pyenv) recommended)

Establish Google Cloud Platform app & credentials with read access to Google Sheets.

1. Copy `.env.sample` to `.env`
2. Set Google Sheet ID and Google API credentials JSON file path in `.env`

## Operation

Generate PDFs in `out/` directory.

```console
$ make
```

## Development

See https://github.com/jazzband/pip-tools for specifying requirements in `requirements.in` and `requirements-dev.in`.

```console
$ make pip-sync-dev
```

See the [Makefile](Makefile) for additional useful make targets.

## License

This software is licensed under the permissive [MIT license](LICENSE).
