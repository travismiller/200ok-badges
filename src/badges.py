import click
import math

from copy import copy
from googleapiclient.discovery import build
from PyPDF2 import PdfFileWriter, PdfFileReader

from reportlab.lib.colors import CMYKColor
from reportlab.lib.pagesizes import LETTER
from reportlab.lib.units import inch
from reportlab.pdfbase import pdfmetrics
from reportlab.pdfbase.ttfonts import TTFont
from reportlab.pdfgen.canvas import Canvas

from tempfile import NamedTemporaryFile
from typing import Iterator

from .settings import BADGES_SHEET_ID, GOOGLE_CREDENTIALS, PRIVATE_TOKEN_PICKLE
from .utils import google_creds


TEMPLATE_PATH = "templates/2019/Badge_6up_Avery74459_Blank.pdf"
BADGE_LAYOUT = {
    "size": LETTER,
    "width": 4 * inch,
    "height": 3 * inch,
    "top": 1 * inch,
    "left": 0.25 * inch,
    "cols": 2,
    "rows": 3,
}
FONTS = {
    "League_Spartan_Bold": "fonts/LeagueSpartan-Bold.ttf",
    "Gobold_Bold_Regular": "fonts/Gobold Bold.ttf",
}
COLORS = {
    "BLACK": CMYKColor(0, 0, 0, 1),
    "GREY": CMYKColor(0, 0, 0, 0.6),
    "BLUE": CMYKColor(0.78, 0.42, 0, 0),
    "RED": CMYKColor(0, 0.87, 0.81, 0),
}
STYLES = {
    "Default": {
        "name": {"font": ("League_Spartan_Bold", 20), "color": COLORS["BLACK"]},
        "company": {"font": ("Gobold_Bold_Regular", 14), "color": COLORS["GREY"]},
        "handle": {"font": ("League_Spartan_Bold", 14), "color": COLORS["BLUE"]},
    },
    "Attendees": {},
    "Speakers": {},
    "Volunteers": {
        "handle": {"font": ("League_Spartan_Bold", 14), "color": COLORS["RED"]}
    },
}


class Badge:
    LAYOUT = BADGE_LAYOUT

    def __init__(self, info, row, col, style):
        size = self.LAYOUT["size"]
        width = self.LAYOUT["width"]
        height = self.LAYOUT["height"]
        left = self.LAYOUT["left"]
        top = self.LAYOUT["top"]

        self.info = info
        self.row = row
        self.col = col
        self.style = style
        self.x = left + ((col - 1) * width)
        self.y = (size[1] - top) - ((row - 1) * height)

    def __repr__(self):
        x = {
            "name": self.name(),
            "company": self.company(),
            "handle": self.handle(),
            "style": self.style,
            "row": self.row,
            "col": self.col,
            "x": self.x,
            "y": self.y,
        }
        x = ' '.join([f"{k}='{v}'" for k, v in x.items()])
        return f"<Badge {x}>"

    def name(self):
        return self.info[0]

    def company(self):
        try:
            return self.info[1].upper()
        except IndexError:
            return ""

    def handle(self):
        try:
            return self.info[2]
        except IndexError:
            return ""

    def draw(self, c: Canvas):
        """
        Draw badge on the canvas
        :param c: Canvas
        :return:
        """
        info = self.info
        tab = self.style
        width = self.LAYOUT["width"]
        x = self.x + (width / 2)
        y = self.y - (1.25 * inch)

        c.setFillColor(style_get(tab, "name", "color"))
        c.setFont(*style_get(tab, "name", "font"))
        c.drawCentredString(x, y, info[0])

        if len(info) > 1:
            c.setFillColor(style_get(tab, "company", "color"))
            c.setFont(*style_get(tab, "company", "font"))
            c.drawCentredString(x, y - (0.5 * inch), info[1].upper())

        if len(info) > 2:
            c.setFillColor(style_get(tab, "handle", "color"))
            c.setFont(*style_get(tab, "handle", "font"))
            c.drawCentredString(x, y - (1 * inch), info[2])


def dict_get(d, keys):
    x = d
    for key in keys:
        try:
            x = x[key]
        except KeyError:
            return None
    return x


def style_get(tab, field, style):
    return dict_get(STYLES, (tab, field, style)) or dict_get(
        STYLES, ("Default", field, style)
    )


def read_sheet(tab) -> Iterator[str]:
    """
    Gets rows from a Google Sheet.

    Assumes columns: Name, Company, Handle
    :param tab:
    :return:
    """
    SCOPES = ["https://www.googleapis.com/auth/spreadsheets.readonly"]
    service = build(
        "sheets",
        "v4",
        credentials=google_creds(SCOPES, GOOGLE_CREDENTIALS, PRIVATE_TOKEN_PICKLE),
    )
    sheet = service.spreadsheets()
    result = (
        sheet.values().get(spreadsheetId=BADGES_SHEET_ID, range=f"{tab}!A1:C").execute()
    )
    rows = result.get("values", [])
    return rows[1:]


def load_template() -> PdfFileReader:
    template_pdf = PdfFileReader(open(TEMPLATE_PATH, "rb"))
    return template_pdf


def register_fonts():
    """
    Register Fonts
    :return:
    """
    for k, v in FONTS.items():
        pdfmetrics.registerFont(TTFont(k, v))


def generate(tab: str, template_page: int):
    template = load_template()
    background = template.getPage(template_page)

    output = PdfFileWriter()

    with NamedTemporaryFile() as file:
        c = Canvas(file.name)
        cols = Badge.LAYOUT["cols"]
        rows = Badge.LAYOUT["rows"]
        for i, info in enumerate(read_sheet(tab)):
            page_i = i % (cols * rows)
            row = math.ceil((page_i + 1) / cols)
            col = (i % cols) + 1

            if i > 0 and page_i == 0:
                c.showPage()

            badge = Badge(info, row, col, tab)
            badge.draw(c)
            print(badge)

        c.showPage()
        c.save()

        overlay = PdfFileReader(file)
        for i in range(overlay.getNumPages()):
            page = copy(background)
            page.mergePage(overlay.getPage(i))
            page.compressContentStreams()
            output.addPage(page)

        with open(f"out/{tab}.pdf", "wb") as output_file:
            output.write(output_file)


@click.group()
def cli():
    """A simple command line tool."""


@cli.command()
def attendees():
    """Attendees…"""
    generate("Attendees", 1)


@cli.command()
def speakers():
    """Speakers…"""
    generate("Speakers", 2)


@cli.command()
def volunteers():
    """Volunteers…"""
    generate("Volunteers", 0)


def main():
    register_fonts()
    cli()


if __name__ == "__main__":
    main()
