import os
from dotenv import load_dotenv

load_dotenv()

BADGES_SHEET_ID = os.getenv("BADGES_SHEET_ID")
GOOGLE_CREDENTIALS = os.getenv("GOOGLE_CREDENTIALS")
PRIVATE_TOKEN_PICKLE = ".private/token.pickle"
