SHELL = /bin/bash

CODE ?= src tests
VENV_BIN ?= .venv/bin/
PYTHON ?= $(VENV_BIN)python
PIP_COMPILE ?= $(VENV_BIN)pip-compile
PIP_SYNC ?= $(VENV_BIN)pip-sync
BLACK ?= $(VENV_BIN)black
MYPY ?= $(VENV_BIN)mypy
PYTEST ?= $(VENV_BIN)pytest

.PHONY: default attendees speakers volunteers clean venv lint reformat tests

default: attendees speakers volunteers

attendees:
	$(PYTHON) -m src.badges attendees

speakers:
	$(PYTHON) -m src.badges speakers

volunteers:
	$(PYTHON) -m src.badges volunteers

clean:
	find $(CODE) -iname '*.pyc' -exec rm {} \;
	find $(CODE) -iname '__pycache__' -exec rm -r {} \;
	rm -rf .mypy_cache .pytest_cache

venv: .venv

pip-sync: requirements.txt
	$(PIP_SYNC) requirements.txt

pip-sync-dev: requirements-dev.txt requirements.txt
	$(PIP_SYNC) requirements-dev.txt requirements.txt

lint:
	$(BLACK) --check -- $(CODE)
	$(MYPY) -- $(CODE)

reformat:
	$(BLACK) -- $(CODE)

tests:
	$(PYTEST)

# .REAL

.venv:
	[ -d .venv ] || python3 -m venv .venv
	$(PYTHON) -m pip install -U pip setuptools wheel pip-tools
	touch .venv

requirements.txt: venv
	$(PIP_COMPILE) --generate-hashes requirements.in

requirements-dev.txt: venv
	$(PIP_COMPILE) --generate-hashes requirements-dev.in
